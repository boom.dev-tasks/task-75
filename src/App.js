import "./App.css";
import React from "react";

function App() {

  const savedState = localStorage.getItem('state');
  const [state, setState] = React.useState(savedState ? savedState : '');

  function SaveNote() {
    localStorage.setItem('state', state);
    console.log(localStorage.state);
  }

  function ClearNote() {
    localStorage.clear();
    setState("");
  }
  
  return (
    <div className="App">
      <div className="box">
        <div className="field">
          <div className="control">
            <textarea className="textarea is-large" value={state} onChange={(e) => { setState(e.target.value) }} placeholder="Notes..." />
          </div>
        </div>
        <button onClick={SaveNote} className="button is-large is-primary is-active">Save</button>
        <button onClick={ClearNote} className="button is-large">Clear</button>
      </div>
    </div>
  );
}

export default App;
